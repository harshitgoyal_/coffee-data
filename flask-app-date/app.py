from flask import Flask, request
from date_extractor import test
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/")
def get_data():
  return "working"

@app.route("/get_date", methods=['POST'])
def get_date():
  req_data = request.get_json()
  sentence = req_data['content'] 
  # print(test(sentence))
  return {"dates" : test(sentence)}

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=3000)