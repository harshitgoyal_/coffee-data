from word2number import w2n
import datetime
from dateutil.relativedelta import relativedelta
import re
import dateparser
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from calendar import monthrange
from dateparser.search import search_dates

stem_months = {'januari' :"january",'februari' : "february",'march' : 'march','april' : 'april','may':'may','june':'june','juli':'july','august':'august','septemb' :'september','octob':'october','novemb':'november','decemb':'december'}
quart = ['first', 'second', 'third', 'fourth']
years = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019,
         2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029]


def last_day_of_month(date_value):
    return date_value.replace(day=monthrange(date_value.year,
                                             date_value.month)[1])


def first_day_of_month(date_value):
    return date_value.replace(day=1)


def convert_word_to_num(sentence):
    new_sent = ''
    for w in sentence.split(" "):
        try:
            num = w2n.word_to_num(w)
            new_sent += str(num) + " "
        except:
            if w in quart:
                new_sent += str(quart.index(w) + 1) + " "
            else:
                new_sent += str(w) + " "
    return new_sent


def get_number(sentence):
    sentence = convert_word_to_num(sentence)
    arr = re.findall('[0-9]+', sentence)
    return_arr = []
    for elements in arr:
        return_arr.append(int(elements))
    return return_arr


def quarters(sentence):
    # checking for first/last keyword
    # stem the input
    if 'past' in sentence or 'previous' in sentence:
        temp_sent = sentence
        temp_sent = temp_sent.replace('past', "")
        quart_arr = get_number(temp_sent)
        no_of_quart = 1
        for w in quart_arr:
            if w in years:
                quart_arr.remove(w)
        if len(quart_arr) > 0:
            no_of_quart = quart_arr[0]
        current_time = datetime.datetime.now()
        start_date = current_time - datetime.timedelta(30*no_of_quart*3)
        start_date = start_date.replace(day=1)
        end_date = datetime.datetime.now()
        end_date = last_day_of_month(end_date)
        return [start_date, end_date]
    elif 'this' in sentence:
        temp_sent = sentence
        temp_sent = temp_sent.replace('this', "")
        quart_arr = get_number(temp_sent)
        no_of_quart = 1
        for w in quart_arr:
            if w in years:
                quart_arr.remove(w)
        if len(quart_arr) > 0:
            no_of_quart = int(quart_arr[0])
        year = datetime.datetime.now().year
        temp_month = datetime.datetime.now().month
        if(temp_month >= 1 and temp_month < 4):
            temp_month = 1
        elif(temp_month >= 4 and temp_month < 7):
            temp_month = 4
        elif (temp_month >= 7 and temp_month < 10):
            temp_month = 7
        else:
            temp_month = 10
        current_time = datetime.datetime.now()
        start_date = current_time.replace(month=temp_month, day=1,
                                          year=int(year))
        end_date = current_time + datetime.timedelta(30*no_of_quart*3)
        end_date = last_day_of_month(end_date)
        return [start_date, end_date]
    elif 'next' in sentence or 'following' in sentence:
        temp_sent = sentence
        temp_sent = temp_sent.replace('next', "")
        quart_arr = get_number(temp_sent)
        no_of_quart = 1
        for w in quart_arr:
            if w in years:
                quart_arr.remove(w)
        if len(quart_arr) > 0:
            no_of_quart = int(quart_arr[0])
        year = datetime.datetime.now().year
        temp_month = datetime.datetime.now().month
        if(temp_month >= 1 and temp_month < 4):
            temp_month = 4
        elif(temp_month >= 4 and temp_month < 7):
            temp_month = 7
        elif (temp_month >= 7 and temp_month < 10):
            temp_month = 10
        else:
            temp_month = 1
            year += 1
        current_time = datetime.datetime.now()
        start_date = current_time.replace(month=temp_month, day=1,
                                          year=int(year))
        end_date = start_date + relativedelta(months=2)
        end_date = last_day_of_month(end_date)
        return [start_date, end_date]
    elif 'first' in sentence:
        temp_sent = sentence
        temp_sent = temp_sent.replace('first', "")
        year = datetime.datetime.now().year
        quart_arr = get_number(temp_sent)
        months = 3
        for w in quart_arr:
            if w in years:
                year = w
                quart_arr.remove(w)
        if len(quart_arr) > 0:
            months = (int(quart_arr[0])) * 3
        current_time = datetime.datetime.now()
        start_date = current_time.replace(month=1, day=1, year=int(year))
        last_day_temp = current_time.replace(month=int(months),
                                             day=1, year=int(year))
        end_date = last_day_of_month(last_day_temp)
        return [start_date, end_date]
    elif 'last' in sentence:
        temp_sent = sentence
        temp_sent = temp_sent.replace('last', "")
        year = datetime.datetime.now().year
        quart_arr = get_number(temp_sent)
        for w in quart_arr:
            if w in years:
                quart_arr.remove(w)
                year = int(w)
        months = 3
        if len(quart_arr) > 0:
            months = (int(quart_arr[0])) * 3
        end_date = datetime.datetime.now().replace(month=12, day=31, year=year)
        start_date = end_date - datetime.timedelta(30*months)
        start_date = first_day_of_month(start_date)
        return [start_date, end_date]
    elif 'all' in sentence:
        temp_sent = sentence
        temp_sent = temp_sent.replace('last', "")
        year = datetime.datetime.now().year
        quart_arr = get_number(temp_sent)
        for w in quart_arr:
            if w in years:
                quart_arr.remove(w)
                year = int(w)
        start_date = datetime.datetime.now().replace(month=1, day=1, year=year)
        end_date = datetime.datetime.now().replace(month=12, day=31, year=year)
        return [start_date, end_date]
    else:
        year = datetime.datetime.now().year
        try:
            quart = 1
            quart_arr = get_number(sentence)
            for w in quart_arr:
                if w in years:
                    quart_arr.remove(w)
                    year = int(w)
            if len(quart_arr) > 0:
                quart = quart_arr[0]
            if quart == 1:
                current_time = datetime.datetime.now()
                start_date = current_time.replace(month=1, day=1, year=year)
                end_date = (current_time.replace(month=3, day=1, year=year))
                end_date = last_day_of_month(end_date)
            else:
                current_time = datetime.datetime.now()
                start_date = current_time.replace(month=3*(int(quart)-1) + 1,
                                                  day=1, year=year)
                end_date = current_time.replace(month=3*(int(quart)),
                                                day=6, year=year)
                end_date = last_day_of_month(end_date)
        except Exception as err:
            print(err)
            return "some error occurred", "some error occurred"
        return [start_date, end_date]
    return


def get_date(sentence, one_date_flag=False):
    main_sent = sentence
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(sentence)
    filtered_sentence = [w for w in word_tokens if not w.lower() in stop_words]

    sentence = ""
    sentence = " ".join(filtered_sentence)
    ps = PorterStemmer()
    temp_arr = []
    for w in word_tokenize(sentence):
        temp_arr.append(ps.stem(w))
    ending = set(['end', 'last', 'final'])
    starting = set(['start', "begin", "till"])
    total = set(list(ending) + list(starting))
    intersection = set(temp_arr).intersection(total)
    try:
        if one_date_flag:
            # means there is only on vfr5e date available in the sentence
            # and we need to get both the dates
            date_t = dateparser.parse(sentence)
            print(date_t, "Todays date")
            start_date = first_day_of_month(date_t)
            end_date = last_day_of_month(date_t)
            return [start_date, end_date]
        if len(list(intersection)) == 0:
            date_t = dateparser.parse(sentence)
            # print(first_day_of_month(date_t),"XXXXXXXXX")
            # print(" xxxx SECOND")
            return [first_day_of_month(date_t)]
        else:
            temp_arr.remove(list(intersection)[0])
            for i, word in enumerate(temp_arr):
                try:
                    month = stem_months[word]
                    temp_arr[i] = month
                except Exception:
                    continue
            sentence = " ".join(temp_arr)
            if list(intersection)[0] in ending:
                date_t = dateparser.parse(sentence)
                return [last_day_of_month(date_t)]
            elif list(intersection)[0] in starting:
                date_t = dateparser.parse(sentence)
                # print(" xxxx 3")
                return [first_day_of_month(date_t)]
    except Exception as err:
        print("error occurred ", main_sent, "  ", err)
        return []


def dateOptimizer(start_dt, end_dt, min_dt, max_dt):
    min_dt = datetime.datetime.strptime(min_dt, '%Y-%m-%d')
    max_dt = datetime.datetime.strptime(max_dt, '%Y-%m-%d')
    if start_dt < min_dt:
        start_dt = min_dt
    if start_dt > max_dt:
        start_dt = min_dt
    if end_dt < min_dt:
        end_dt = min_dt
    if end_dt > max_dt:
        end_dt = max_dt
    r_date = (str(start_dt), str(end_dt))
    return r_date


def test(temp_ent):
    '''Tests the model using the specified data'''
    local_dates_arr = []
    min_dt = temp_ent["min_date"]
    max_dt = temp_ent["max_date"]
    del temp_ent["min_date"]
    del temp_ent["max_date"]
    if len(temp_ent) == 1:
        # means wither we found quarter or the last date
        if "quarter" in temp_ent['start date']:
            for key in temp_ent.keys():
                ent_text = temp_ent[key]
                dates = quarters(ent_text)
                return dateOptimizer(dates[0], dates[1], min_dt, max_dt)
        else:
            # we need to get the first date and last date
            for key in temp_ent.keys():
                ent_text = temp_ent[key]
                dates = get_date(ent_text, True)
                if len(dates) > 1:
                    for dt in dates:
                        local_dates_arr.append(dt)
                else:
                    local_dates_arr.append(dates[0])
            if len(local_dates_arr) == 0:
                try:
                    for key in temp_ent.keys():
                        ent_text = temp_ent[key]
                    date = search_dates(ent_text)
                    year = date[0][0]
                    if year is not None:
                        start_date = str(datetime.datetime.now().replace(month=1, day=1, year=int(year)))
                        last_day_temp = str(datetime.datetime.now().replace(month=12, day=31, year=int(year)))
                        dates = [str(start_date),str(last_day_temp)]
                        return dateOptimizer(dates[0],dates[1],min_dt,max_dt)
                except Exception as error:
                    print(error)
                    return []
            return dateOptimizer(local_dates_arr[0],
                                 local_dates_arr[1],
                                 min_dt, max_dt)
    else:
        for key in temp_ent.keys():
            ent_text = temp_ent[key]
            dates = get_date(ent_text)
            if len(dates) > 0:
                local_dates_arr.append(dates[0])
        if len(local_dates_arr) == 0:
            try:
                date = search_dates(temp_ent)
                year = date[0][0]
                if year is not None:
                    current_date = datetime.datetime.now().date()
                    start_date = str(current_date.replace(month=1,
                                                          day=1,
                                                          year=int(year)))
                    last_day_temp = str(current_date.replace(month=12,
                                                             day=31,
                                                             year=int(year)))
                    dates = [str(start_date), str(last_day_temp)]
                    return dateOptimizer(dates[0], dates[1], min_dt,max_dt)
            except Exception:
                return []
        return dateOptimizer(local_dates_arr[0], local_dates_arr[1],
                             min_dt, max_dt)
    return "NULL FROM HERE"
