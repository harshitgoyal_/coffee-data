let autocompleteData = {}
let autoCompleteInstance = null

$(document).ready(function(){
    $('input.autocomplete').autocomplete({
        data: autocompleteData,
    });

    autoCompleteInstance = M.Autocomplete.getInstance($('input.autocomplete'))

    // bind event to fetch auto-complete suggestions
    $('input.autocomplete').on('input', function() {
        fetchSuggestions()
    }) 

    // bind event to search report
    $('.fetch-report-btn').on('click', function() {
        fetchReport()
    })
});

const fetchSuggestions = async () => {
    const inputData = $('input.autocomplete').val()
    try {
        const autocompleteSuggestions = await fetch(`/search/names?query=${inputData}`)
        const results = await autocompleteSuggestions.json()
        autocompleteData = {}
        results.suggestions.map(result => {
            autocompleteData[result.data] = null
        })
        autoCompleteInstance.updateData(autocompleteData)
    } catch(error) { }
}

const fetchReport = async () => {
    const searchQuery = $('input.autocomplete').val()
    try {
        $('.loading-spinner').removeClass('hidden')
        const reportRequestSettings = {
            "url": "/getData",
            "method": "POST",
            "headers": {
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "sentence": searchQuery
            }),
        };
        const reportsDataResponse = await $.ajax(reportRequestSettings)
        populateReportsTable(reportsDataResponse)
        $('.loading-spinner').addClass('hidden')
    } catch(error) {    
        $('.loading-spinner').addClass('hidden')
    }
}

const populateReportsTable = (reportData) => {
    const tableHeadRow = $('.report-table-head > tr')
    const tableBody = $('.report-table-body')
    
    $('.report-data-container').removeClass('hidden')
    tableHeadRow.empty()
    tableBody.empty()

    // populate the table head
    reportData.columns.map(column => {
        tableHeadRow.append(`<th>${column}</th>`)
    })

    // populate the table body
    reportData.data.map(rowData => {
        let singleRowTableData = ''
        rowData.forEach(cellData => {
            singleRowTableData += `<td>${cellData}</td>`
        })
        tableBody.append(`<tr>${singleRowTableData}</tr>`)
    })
    $('.report-start-date').text(reportData.start_date ? reportData.start_date : 'N/A')
    $('.report-end-date').text(reportData.end_date ? reportData.end_date : 'N/A')
}



