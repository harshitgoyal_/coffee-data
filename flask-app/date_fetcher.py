import json
import requests
import dateparser


def fetch_date(data):
    url = "http://localhost:3000/get_date"
    payload = json.dumps({
        "content": data
    })
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    try:
        date_time_arr = response.json()['dates']
    except Exception:
        date_time_arr = response.json()['date']
    start_date = ""
    end_date = ""
    try:
        if date_time_arr is not None:
            if len(date_time_arr) == 1:
                start_date = dateparser.parse(date_time_arr[0]).date()
                end_date = dateparser.parse(date_time_arr[0]).date()
            else:
                start_date = dateparser.parse(date_time_arr[0]).date()
                end_date = dateparser.parse(date_time_arr[1]).date()
            return start_date, end_date
    except Exception:
        return None, None
