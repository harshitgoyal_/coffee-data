import pymssql
import pandas as pd
import us


def get_data(subject, start_dte, end_dte, region, sim_sub, sp_type):
    if ('sale' in subject):
        print("Similarity subject", sim_sub, "Similarity type", sp_type)
        return fetch_sales_db_data(subject, start_dte, end_dte, region)
    elif("order" in subject):
        print("Similarity subject", sim_sub, "Similarity type", sp_type)
        return fetch_sales_db_data(subject, start_dte, end_dte, region)
    elif("product" in subject):
        print("Similarity subject", sim_sub, "Similarity type", sp_type)
        return fetch_sales_db_data(subject, start_dte, end_dte, region)
    else:
        print("Sp not detected")


def fetch_sales_db_data(subject, start_dte, end_dte, region):
    try:
        state = us.states.lookup(region)
        reg = state.abbr
        print("region", reg)
    except Exception as error:
        print(error, " US state lookup error")
    try:
        server = 'database.sunwaretechnologies.com'
        database = 'Retail360v1_6_Mfg'
        username = 'codaadmin'
        password = 'Coffee@4300!'
        conn = pymssql.connect(server=f'{server}', user=f'{username}',
                               password=f'{password}', database=f'{database}')
        query = '''EXEC [Retail360v1_6_Mfg].dbo.usp_Sales_ByDate_ByRegion '%s', '%s','%s','%s' ''' %(subject, start_dte, end_dte, reg)
        SQL_Query = pd.read_sql_query(
            query, conn)
        df = pd.DataFrame(SQL_Query)
    except Exception as err:
        print(err, " Database server error")
        df = pd.DataFrame()
    return df
