from flask import Flask, request, render_template, jsonify
from numpy.core.numeric import False_
from sklearn.metrics.pairwise import cosine_similarity
from numpy import dot
from numpy.linalg import norm
from date_fetcher import fetch_date
from get_entities import fetch_ent
from similarity import fetch_similarity
from store_procedures import get_data
# library to do permutation and combination
from itertools import permutations
# elastic search
from fast_autocomplete import AutoComplete
# Levenshtein
from Levenshtein import ratio
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

"""## Libraries"""
"""### Sales data"""

_words_r = {}
dictionary_r = {}
dict_r = {}


def calculatePermutations(sentence):
    """
        produce the different patterns of the sentence
        input: Sentence (String)
        output: list of strings["sky is blue","blue is sky","is blue sky"...]
    """
    lis = list(sentence.split())
    permute = permutations(lis)
    arr_of_words = []
    for i in permute:
        permutelist = list(i)
        word = ""
        for j in permutelist:
            word += j + " "
        arr_of_words.append(word.strip())
    return arr_of_words

def lower_dict_keys(dict_):
    """
        converting everything to lower case 
        input: dictionary
        ouput: dictionary
    """
    lower_dict_ = []
    for dict_ in dict_:
        new_dict = {}
        for letter in dict_:
            number = dict_[letter]
            new_dict.update({letter.lower(): number})
        lower_dict_.append(new_dict)
    return lower_dict_


def attach_word_with_table(dict_):
    """
      attach all the words with all the tables it belongs to
      eg- "sales" -> ["sales_trend","avg_dollar_per_transaction"]
      input: dictionary
      output: dictionary
    """
    huge_dict = {}
    for arr in dict_:
        for element in arr:
            value = arr[element]
            for randomized_sent in [element]:
                if randomized_sent in huge_dict:
                    get_value = huge_dict[randomized_sent]
                    if value not in get_value:
                        temp_arr = get_value + [value]
                        huge_dict[randomized_sent] = temp_arr
                    else:
                        huge_dict[randomized_sent] = [value]
                else:
                    huge_dict[randomized_sent] = [value]
    return huge_dict


def attach_permuted_with_correct(dict_):
    """
      attach the permuted sentence with the exact sentence
        eg - {'analysis basket': 'basket analysis',
              'basket analysis' : 'basket analysis'}
                        here analysis basket is permuted of basket analysis
      input: dictionary
      output: dictionary
    """
    dictionary = {}
    for arr in dict_:
        for element in arr:
            temp_arr = calculatePermutations(element)
            for temp in temp_arr:
                dictionary[temp] = temp_arr[0]
    return dictionary


def select_table(index):
    global dict_r
    global dictionary_r
    global _words_r
    global sales_words
    global product_words
    global order_words
    global sales_pnc_correction_dict
    global product_pnc_correction_dict
    global order_pnc_correction_dict
    global sales_table_dict
    global product_table_dict
    global order_table_dict

    _words_r = sales_words
    dictionary_r = sales_pnc_correction_dict
    dict_r = sales_table_dict

    if index == 1:
        _words_r = product_words
        dictionary_r = product_pnc_correction_dict
        dict_r = product_table_dict
    elif index == 2:
        _words_r = order_words
        dictionary_r = order_pnc_correction_dict
        dict_r = order_table_dict

    return _words_r, dictionary_r, dict_r


def main(_sentence, _words, dictionary):
    '''
      perform the elastic search
      input :
          1. _sentence - input sentence which we get from user (String)
          2. _words - dataset to search from (Dictionary)
          3. dictionary - correct result from permuted sentence (Dictionary)
    '''
    autocomplete = AutoComplete(words=_words)
    words = _sentence.split(' ')
    length = len(words)
    word = words[length - 1]
    results = autocomplete.search(word=word, max_cost=1, size=10)

    # Prepare the suggestions base string
    base_string = ''
    for i in range(0, length - 2):
        base_string = ('{} {}').format(base_string, words[i])

    base_string.strip(' ')

    suggestions = []
    for result in results:
        text = base_string + ' ' + result[0]
        suggestions.append(text.strip(' '))
    suggest = []
    for suggestion in suggestions:
        suggest.append(dictionary[suggestion])
    return list(set(suggest))


def get_the_suggested_data(sent_arr, arr, count):
    suggested_data = []
    sent_wo_last_word = " ".join(sent_arr[:-count])
    sent_wo_last_word = sent_wo_last_word.strip()
    for w in arr:
        suggested_data.append(sent_wo_last_word + " " + w)
    return suggested_data


previous_suggested_array = []
previous_suggestions = []


# sales data
# sales_data = {}
sales_table_dict = {}
sales_pnc_correction_dict = {}
sales_words = {}


# product_data = {}
product_table_dict = {}
product_pnc_correction_dict = {}
product_words = {}

# order_data = {}
order_table_dict = {}
order_pnc_correction_dict = {}
order_words = {}
sales_words = {}
product_words = {}
order_words = {}


@app.route("/")
def index():
    global sales_data
    global sales_table_dict
    global sales_pnc_correction_dict
    global sales_words

    global product_data
    global product_table_dict
    global product_pnc_correction_dict
    global product_words

    global order_data
    global order_table_dict
    global order_pnc_correction_dict
    global order_words

    global _words
    global sales_words
    global product_words
    global order_words
    # Sales data
    sales_data = lower_dict_keys(sales_data)
    sales_table_dict = attach_word_with_table(sales_data)
    sales_pnc_correction_dict = attach_permuted_with_correct(sales_data)
    # words for elastic search
    for word in sales_pnc_correction_dict:
        sales_words[word] = {}

    # product data
    product_data = lower_dict_keys(product_data)
    product_table_dict = attach_word_with_table(product_data)
    product_pnc_correction_dict = attach_permuted_with_correct(product_data)

    # words for elastic search

    for word in product_pnc_correction_dict:
        product_words[word] = {}

    # order data
    order_data = lower_dict_keys(order_data)
    order_table_dict = attach_word_with_table(order_data)
    order_pnc_correction_dict = attach_permuted_with_correct(order_data)
    # words for elastic search

    for word in order_pnc_correction_dict:
        order_words[word] = {}
        order_words[0] = 10
        order_words[1] = 20
        order_words[2] = 30
    return render_template("index.html")


def get_suggestion(_sentence):
    global previous_suggestions
    global previous_suggested_array
    global time_filter_data
    global types
    sent_arr = _sentence.split(" ")
    last_word = sent_arr[-1]
    table_index = 0

    _words, correction_dict, tables_info_dict = select_table(index=table_index)
    if len(sent_arr) == 1:
        # Now we need to get to know which table user is trying to get the data
        correct_words = last_word
        l_arr = []
        for index, s_types in enumerate(types):
            l_ratio = ratio(correct_words, s_types)
            l_arr.append(l_ratio)
            table_index = l_arr.index(max(l_arr))
    _words, correction_dict, tables_info_dict = select_table(index=table_index)

    arr = []
    counter = len(sent_arr)
    count = 2
    remover_counter = 0
    if len(previous_suggestions) > 0:
        last_word = sent_arr[-1]
        # suggest years
        flag = False
        time_suggest_arr = []
        for dicto in time_filter_data:
            for iter in time_filter_data[dicto]:
                if last_word in iter:
                    if ratio(last_word, iter) == 1:
                        flag = True
                    time_suggest_arr.append(iter)

        if flag:
            sent = " ".join(sent_arr)
        if len(time_suggest_arr) > 0:
            query_with_time = []
            for t in time_suggest_arr:
                full_t_query = sent_arr[:-1]
                f_sent = " ".join(full_t_query) + " " + str(t)
                query_with_time.append(f_sent)
            return query_with_time
        else:
            # Remove last word
            sent = " ".join(sent_arr)
            return [sent]
    else:
        while(len(arr) == 0):
            arr = main(_sentence=last_word, _words=_words,
                       dictionary=correction_dict)
            if len(arr) > 0:
                remover_counter += 1
                previous_suggested_array = arr
                suggestions = get_the_suggested_data(sent_arr, arr,
                                                     remover_counter)
                previous_suggestions = suggestions
                return suggestions

            # Now we need to get the
            if count <= counter:
                last_word = sent_arr[-1 * count]
                count += 1
                remover_counter += 1
                continue

            # If counter considered all teh words
            if count > counter:
                # not get the first character of the first word
                last_word = sent_arr[0][0]


@app.route("/getData", methods=['post'])
def fetch_data():
    data = request.get_json()
    original_sentence = data['sentence']
    # table we need to look up
    print(original_sentence)
    sentence = original_sentence.strip()
    date_ent, temp_ent, rel_ent = fetch_ent(sentence)
    print(f"FETCHED ENTITIES {date_ent}, {temp_ent}, {rel_ent}")
    date_ent = date_ent['date']
    temp_ent = temp_ent['temp_ent']
    rel_ent = rel_ent['rel_ent']
    try:
        region = temp_ent["region"]
    except Exception:
        region = "CA"
    try:
        subject = temp_ent["subject"]
    except Exception:
        subject = "sales"
    date_ent["min_date"] = "2016-10-02"
    date_ent["max_date"] = "2019-03-19"

    start_dt = "12/1/1700"
    end_dt = "13/1/1700"
    if date_ent.keys():
        # call date_fetcher.py
        start_dt, end_dt = fetch_date(date_ent)
    print(f"FETCHED DATE {start_dt}, {end_dt}")
    # return sp data
    sim_sub, sp_type = fetch_similarity(sentence)
    print(f"FETCHED SIMILARITY {sim_sub}, {sp_type}")
    df = get_data(subject, start_dt, end_dt, region, sim_sub, sp_type)
    print("FETCHED DATA FROM SP")
    result = df.to_json(orient="split")
    response = json.loads(result)
    response['end_date'] = str(end_dt)
    response['start_date'] = str(start_dt)
    return response


@app.route("/search/<string:box>")
def process(box):
    query = request.args.get('query')
    query = query.strip()
    suggestions = []
    global previous_suggested_array
    global previous_suggestions
    global time_suggest_arr
    try:
        if box == 'names':
            # do some stuff to open your names text file
            # do some other stuff to filter
            # put suggestions in this format...
            suggestions = []
            if len(list(query)) > 2:
                prev_item_ref = []
                if len(previous_suggested_array) > 0:
                    for ele in previous_suggested_array:
                        if query in ele:
                            prev_item_ref.append(ele)
                        else:
                            prev_item_ref = []
                    if len(prev_item_ref) > 0:
                        for data in prev_item_ref:
                            suggestions.append({'value': data, 'data': data})
                        return jsonify({"suggestions": suggestions})
                try:
                    suggested_data_ = {}
                    if len(query) > 2:
                        suggested_data_ = (get_suggestion(str(query)))
                    else:
                        suggested_data_ = ["write more"]
                except Exception as e:
                    return str(e)
                if suggested_data_ is not None:
                    for data in suggested_data_:
                        suggestions.append({'value': data, 'data': data})
            else:
                previous_suggested_array = []
                previous_suggestions = []
                time_suggest_arr = []
                suggestions = []
        return jsonify({"suggestions": suggestions})
    except Exception:
        return jsonify({"suggestions": "suggestions"})


sales_data = []
product_data = []
order_data = []
types = []
time_filter_data= {}
if __name__ == "__main__":

    # sbert_model = SentenceTransformer('bert-base-nli-mean-tokens')
    sales_trend = {"Sales-Goal" : "sales_trend", "Sales" : "sales_trend","Traffic" : "sales_trend","netsales" : "sales_trend","Salesgap" : "sales_trend"}
    avg_dollar_per_transaction = {"Basket-Analysis" : "avg_dollar_per_transaction","ADT" : "avg_dollar_per_transaction" ,"Maximum" : "avg_dollar_per_transaction","Upper Quartile" : "avg_dollar_per_transaction","Median" : "avg_dollar_per_transaction","Lower Quartile" : "avg_dollar_per_transaction","Minimum" : "avg_dollar_per_transaction"}
    sales_accross_USA = {"Calender-Date" : "sales_accross_USA","Year-Num" : "sales_accross_USA","QuarterNum" : "sales_accross_USA","QuarterDesc" : "sales_accross_USA","MonthNum" : "sales_accross_USA","MonthDesc" : "sales_accross_USA","WeekNum" : "sales_accross_USA","DayNum" : "sales_accross_USA","Sales" : "sales_accross_USA","SalesGoal" : "sales_accross_USA","StoreKey" : "sales_accross_USA","StoreName" : "sales_accross_USA","GeoLat" : "sales_accross_USA","GeoLat" : "sales_accross_USA","SalesColor" : "sales_accross_USA"}
    Sales_Analysis_By_Store  = {"Sales" : "Sales_Analysis_By_Store","Sales-Goal" : "Sales_Analysis_By_Store","Returns" : "Sales_Analysis_By_Store","netsales" : "Sales_Analysis_By_Store"}
    traffic_trend = {"Traffic" : "traffic_trend","Sales" : "traffic_trend","Sales-Goal" : "traffic_trend","Sales Variance" : "traffic_trend"}
    Sales_Goal_VS_Actual = {"Sale": "Sales_Goal_VS_Actual"}
    adt = {'Transaction-Avg' : "adt"}
    Sales_Revenue = {"Revenue-By-Sq.ft" : "Sales_Revenue"}
    Sales_Qty_vs_Return_Qty_vs_Void_Qty = {"Sales-Qty" : "Sales_Qty_vs_Return_Qty_vs_Void_Qty","Return-Qty" : "Sales_Qty_vs_Return_Qty_vs_Void_Qty","Void Qty" : "Sales_Qty_vs_Return_Qty_vs_Void_Qty","netsales": "Sales_Qty_vs_Return_Qty_vs_Void_Qty"}


    sales_data = [sales_trend, avg_dollar_per_transaction, sales_accross_USA, Sales_Analysis_By_Store, traffic_trend, Sales_Goal_VS_Actual, adt, Sales_Revenue, Sales_Qty_vs_Return_Qty_vs_Void_Qty]

    """## Product data"""

    product_items = {"Accessories-product" : "product item","Apparel-product" : "product item","Beauty-products" : "product item","Dress-products" : "product item","Baby-products" : "product item","Bags-products" : "product item","footwear-product" : "product item"}
    tax = {"tax growth on products" : "Tax","tax change on products" : "Tax","footwear products tax " : "Tax","water product tax" : "Tax"}
    product_data = [product_items, tax]

    """## Order Data"""

    orders = {"amount-of-orders":"order","cancelled-orders ":"order","booked-orders":"order"}
    order_data = [orders]

    """## Common data"""

    types = ["sales", "product", "order"]

    # time dictionary
    time_filter_data = {"year": ["2015", "2016", "2017",
                                 "2018", "2019", "2020"],

                        "months": ["january", "february", "march", "april",
                                   "may", "june", "july", "august",
                                   "september", "october", "november",
                                   "december"],

                        "day": ["Sunday", "Monday", "Tuesday", "Wednesday",
                                "Thursday", "Friday", "Saturday"],

                        "timeframe": ["quarter 1", "quarter 2",
                                      "quarter 3", "quarter 4"]
                   }
    previous_suggested_array = []
    previous_suggestions = []
    app.run(debug=False)
