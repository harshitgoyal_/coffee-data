from logging import error
from flask import Flask, request, render_template, jsonify
import spacy
# make the factory work
from rel_pipe import make_relation_extractor, score_relations
# make the config work
from rel_model import create_relation_model, create_classification_layer
from rel_model import create_instances
import path_definition as path
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/')
def hello_world():
    return "Hello, World!"


@app.route('/get_ent', methods=['POST'])
def get_date():
    req_data = request.get_json()
    sentence = req_data['data']
    print(sentence)
    # try:
    '''Tests the model using the specified data'''
    # temp_ent = {}
    # date_ent = {}
    rel_ent = {}
    document = [sentence]
    for text in document:
        doc = nlp(text)
        temp_ent = {}
        date_ent = {}
        for ent in doc.ents:
            print(ent.label_, "  ",  ent.text)
            # print(str(ent.label_).strip(),"kkk")
            if "DATE" in str(ent.label_).strip():
                # date_ent["start_date"]=ent.text
                if "start date" in date_ent:
                    date_ent["end date"] = ent.text
                    print(date_ent)
                elif "quarter" in date_ent:
                    date_ent["quarter"] = ent.text
                    print(date_ent)
                else:
                    date_ent['start date'] = ent.text
            else:
                temp_ent[ent.label_] = ent.text

    for name, proc in nlp2.pipeline:
        doc = proc(doc)
        for value, rel_dict in doc._.rel.items():
            for e in doc.ents:
                for b in doc.ents:
                    if e.start == value[0] and b.start == value[1]:
                        if rel_dict['SUBJECT-DATE'] > 0.3 or rel_dict['SUBJECT-PREDICATE'] > 0.3 or rel_dict['SUBJECT-REGION'] > 0.3 or rel_dict['SUBJECT-SUBJECT'] > 0.3:
                            max_key = max(rel_dict, key=rel_dict.get)
                            try:
                                if rel_ent[f"{max_key}"]:
                                    rel_ent[f"{max_key}"] = rel_ent[f"{max_key}"].append([e.text, b.text])
                            except Exception:
                                rel_ent[f"{max_key}"] = [[e.text, b.text]]
                                pass

    return {"ent_data": [{"date": date_ent},
                         {"temp_ent": temp_ent},
                         {"rel_ent": rel_ent}]}


if __name__ == "__main__":
    nlp = spacy.load(path._default_model)
    nlp2 = spacy.load(path._default_model_rel)
    app.run(debug=False, host='0.0.0.0', port=7000)
