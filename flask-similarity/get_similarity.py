from numpy import dot
from numpy.linalg import norm
import spacy
from sentence_transformers import SentenceTransformer
import path_definition as paths
sbert_model = SentenceTransformer('bert-base-nli-mean-tokens')


class similarity:
    def __init__(self) -> None:
        self.nlp = spacy.load(paths.entity_model)
        # SP META_DATA
        self.meta_data = {
          'product': ['SUBJECT for DATE by REGION', 'SUBJECT for DATE',
                      'SUBJECT by REGION'],
          'sales': ['SUBJECT for DATE by REGION', 'SUBJECT for DATE',
                    'SUBJECT by REGION'],
          "sales gap": ['SUBJECT for DATE by REGION', 'SUBJECT for DATE',
                        'SUBJECT by REGION'],
          "orders":  ['SUBJECT for DATE by REGION', 'SUBJECT for DATE',
                      'SUBJECT by REGION'],
          "sales trend": ['SUBJECT for DATE by REGION', 'SUBJECT for DATE',
                          'SUBJECT by REGION']
        }

    def pre_processing(self, text):
        stop_words = ['show', 'me', 'data']
        meta_dict = {}
        original_dict = {}
        for doc in self.nlp.pipe(text, disable=["tagger"]):
            sentence = doc.text
            sub = ""
            for e in doc.ents:
                org_w = str(e.text)
                subj = str(e.label_)
                if subj == "SUBJECT":
                    sub = e.text
                sentence = sentence.replace(org_w, subj)
            tokens_without_sw = [word for word in sentence.split(" ")
                                 if word not in stop_words]
            filtered_sentence = (" ").join(tokens_without_sw)
            if sub != "":
                try:
                    arr = meta_dict[sub]
                    arr.append(filtered_sentence)
                    meta_dict[sub] = arr
                    o_arr = original_dict[sub]
                    o_arr.append(doc.text)
                    original_dict[sub] = o_arr
                except Exception:
                    meta_dict[sub] = [filtered_sentence]
                    original_dict[sub] = [doc.text]
        return meta_dict, original_dict

    def embed_data(self, meta_sentences):
        sent_vec_per_key = []
        for sent in meta_sentences:
            try:
                sent_vec_per_key.append(sbert_model.encode(sent))
            except Exception as err:
                print(f"Error --> {err}")
        return sent_vec_per_key

    def get_similarity(self, user_sub, meta_data_sub):
        usr_sub_emb = self.embed_data(user_sub)[0]
        meta_sub_emb = self.embed_data(meta_data_sub)
        sim_dict = {}
        for index, sent in enumerate(meta_sub_emb):
            sim = dot(usr_sub_emb, sent)/(norm(usr_sub_emb)*norm(sent))
            sim_dict[str(index)] = sim
        max_key = max(sim_dict, key=sim_dict.get)
        return meta_data_sub[int(max_key)], sim_dict

    def get_sp(self, user_txt):
        user_data, usr_org_data = self.pre_processing([user_txt])
        meta_sim_sub, max_key = self.get_similarity(
                                                    list(user_data.keys()),
                                                    list(self.meta_data.keys())
                                                    )
        meta_sentences = self.meta_data[meta_sim_sub]
        sp_typ, sim_dict = self.get_similarity(user_data[
                                                    list(user_data.keys())[0]
                                                    ],
                                               meta_sentences)
        max_key = max(sim_dict, key=sim_dict.get)
        sp_ = list(self.meta_data[meta_sim_sub])[int(max_key)]
        return meta_sim_sub, sp_
