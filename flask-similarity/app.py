from flask import Flask, request
from get_similarity import similarity
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route('/')
def hello_world():
    return "Hello, World!"


@app.route('/get_simmilar', methods=['POST', 'GET'])
def get_data():
    print("fun called")
    req_data = request.get_json()
    sentence = req_data['data']
    # sales SUBJECT for DATE by REGION
    print({"similarity": sim_obj.get_sp(sentence)})
    return {"similarity": sim_obj.get_sp(sentence)}


if __name__ == "__main__":
    sim_obj = similarity()
    app.run(debug=False, host='0.0.0.0', port=8000)
