# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import json
from . import similarity
from . import store_procedure
from . import get_entities
from . import date_fetcher


class ActionSearchSales(Action):
    def name(self) -> Text:
        return "action_search_sales"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        data = tracker.latest_message
        data = str(data['text']).strip()
        date_ent, temp_ent, rel_ent = get_entities.fetch_ent(data)
        date_ent = date_ent['date']
        temp_ent = temp_ent['temp_ent']
        rel_ent = rel_ent['rel_ent']
        try:
            region = temp_ent["region"]
            print("region", region)
        except Exception:
            region = "CA"
        try:
            subject = temp_ent["subject"]
        except Exception:
            subject = "sales"
        date_ent["min_date"] = "2016-10-02"
        date_ent["max_date"] = "2019-03-19"

        start_dt = "12/1/1700"
        end_dt = "13/1/1700"
        if date_ent.keys():
            # call date_fetcher.py
            start_dt, end_dt = date_fetcher.fetch_date(date_ent)

        # return sp data
        sim_sub, sp_type = similarity.fetch_similarity(data)
        df = store_procedure.procedure(subject, start_dt,
                                       end_dt, region, sim_sub, sp_type)
        # df = pd.DataFrame()
        result = df.to_json(orient="split")
        response = json.loads(result)

        data = "start_date == " + str(start_dt) + " end_date == " + str(end_dt) + " data from DB == " + str(response)
        dispatcher.utter_message(text=data)

