import requests
import json


def fetch_ent(sentence):
    url = "http://localhost:7000/get_ent"

    payload = json.dumps({
      "data": sentence
    })
    headers = {
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    ent_data_res = response.json()['ent_data']

    return ent_data_res[0], ent_data_res[1], ent_data_res[2]
