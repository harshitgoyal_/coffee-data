import requests
import json


def fetch_similarity(sentence):
    url = "http://localhost:8000/get_simmilar"

    payload = json.dumps({
      "data": sentence
    })
    headers = {
      'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    sim_data_res = response.json()['similarity']
    return sim_data_res[0], sim_data_res[1]
