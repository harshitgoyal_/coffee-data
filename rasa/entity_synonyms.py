from fuzzywuzzy import process
fuzzy_results = process.extract(
                        token.text, 
                        lookup_data, 
                        processor=lambda a: a['value'] 
                            if isinstance(a, dict) else a, 
                        limit=10)