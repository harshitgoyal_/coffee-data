# from rasa.nlu.components import Component
# from rasa.nlu import utils
# from rasa.nlu.model import Metadata
# from rasa.shared.nlu.training_data import Message
# from spellchecker import SpellChecker
# spell = SpellChecker()

# class CorrectSpelling(Component):

#     name = "Spell_checker"
#     provides = ["message"]
#     requires = ["message"]
#     language_list = ["en"]

#     def __init__(self, component_config=None):
#         super(CorrectSpelling, self).__init__(component_config)

#     def train(self, training_data, cfg, **kwargs):
#         """Not needed, because the the model is pretrained"""
#         pass

#     def persist(self,file_name, model_dir):
#         """Pass because a pre-trained model is already persisted"""
#         pass

#     def process(self, message, **kwargs):
#         try:
#             print("IN 1")
#             if 'text' in message.as_dict():
#                 print("@@@@@@@@@")
#                 mt = message.get('text')
#                 textdata = mt.split()
#                 new_message = ' '.join(spell.correction(w) for w in textdata)
#                 message.set("text", new_message)
#             print(message.as_dict(), "SECOND")
#         except Exception as e:
#             print(e, "error occurred")
#             pass

from rasa.nlu.components import Component
# from rasa.shared.nlu.training_data import Message
import typing
from typing import Any, Optional, Text, Dict
from spellchecker import SpellChecker
spell = SpellChecker()


if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata

from spellchecker import SpellChecker

class SpellCheckerEN(Component):

    provides = ["text"]
    defaults = {}
    language_list = ["en"]

    def __init__(self, component_config=None):
        super(SpellCheckerEN, self).__init__(component_config)

    def process(self, message, **kwargs):
        try:
            print("IN 1")
            if 'text' in message.as_dict():
                mt =  message.get('text')
                textdata = mt.split()
                new_message = ' '.join(spell.correction(w) for w in textdata)
                print(new_message)
                message.set("text", new_message)
        except:
            print("ERROR OCCURED",e)
            pass
    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any
    ) -> "Component":
        if cached_component:
            return cached_component
        else:
            return cls(meta)